This package provides a Gumroad product field for Nova resources.

##Setup
Add your Gumroad access token as `GUMROAD_ACCESS_TOKEN` to your project's .env file.

##Usage
Add the field to your resource's `fields` method.

```php
public function fields(Request $request)
    {
        return [
            \DiskoPete\LaravelNovaGumroad\Fields\Product::make('Product Id')
        ];
    }
```
