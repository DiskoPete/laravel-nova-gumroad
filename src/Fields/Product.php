<?php

namespace DiskoPete\LaravelNovaGumroad\Fields;

use DiskoPete\LaravelGumroad\Services\Api;
use DiskoPete\LaravelGumroad\Support\Product as ProductModel;
use Illuminate\Support\Facades\Cache;
use Laravel\Nova\Fields\Select;

class Product extends Select
{
    public function jsonSerialize(): array
    {

        $this->loadApiProductsAsOptions();

        return parent::jsonSerialize();
    }

    private function loadApiProductsAsOptions(): void
    {

        $products = Cache::remember(
            'gumroad-field-products',
            now()->addHour(),
            fn() => $this->getGumroadApi()->products()
        );

        $this->options(
            $products->map(fn(ProductModel $product) => ['value' => $product->id, 'label' => $product->name])
        );

    }

    private function getGumroadApi(): Api
    {
        return app(Api::class);
    }
}
